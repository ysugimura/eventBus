package com.cm55.eventBus;

import java.lang.ref.*;
import java.util.*;
import java.util.function.*;

/** 
 * ある一つのイベントタイプ（クラス）について、そのリスナーのリストを保持する。
 * リスナーとしては、強参照と弱参照の二種類がある。
 * @author ysugimura
 *
 * @param <T> このホルダが対象とするイベントの型
 */
class ListenerHolder<T> {

  /** イベントタイプ */
  final Class<T>eventType;
    
  /** リスナーリスト */
  ArrayList<Ref<T>>listenerList = new ArrayList<>();    
  
  ListenerHolder(Class<T>eventType) {
    this.eventType = eventType;
  }
  
  /** 
   * リスナーを追加する。
   * 弱参照の場合にはオブジェクトの指定が必須。強い参照の場合は任意
   * @param weak 弱参照を示す
   * @param object オブジェクト
   * @param listener リスナー
   */
  void addListener(boolean weak, Object object, Consumer<T>listener) {
    if (listener == null) throw new NullPointerException("listener should not be null");
    if (weak) {
      if (object == null) throw new NullPointerException("object should not be null");
      listenerList.add(new Weak<T>(object, listener));
    } else {
      listenerList.add(new Strong<T>(object, listener));
    }
  }
  
  /**
   * 指定されたリスナーを削除する
   * @param listener
   */
  void removeListener(Consumer<T>listener) {
    for (int i = listenerList.size() - 1; i >= 0; i--) {
      Consumer<T> l = listenerList.get(i).getListener();
      if (l == null) {
        // 弱参照では既に存在していないかもしれない
        listenerList.remove(i);
        continue;
      }
      if (l == listener) {
        // このリスナーを削除
        listenerList.remove(i);
        return;
      }
    }
  }

  /**
   * イベントをディスパッチする
   * @param event
   */
  List<Consumer<T>>getListeners() {       
    
    // ディスパッチ途中でリストが変更されることがあることに注意。いったん配列化する
    List<Consumer<T>>result = new ArrayList<>();
    listenerList.forEach(ref-> {
      Consumer<T>listener = ref.getListener();
      // 弱参照では既に消えているかもしれない
      if (listener == null) return;
      result.add(listener);
    });
    return result;
  }

  
  /** リスナーへの参照。オブジェクトへの強参照・弱参照がある。 */
  static abstract class Ref<T> {    
    
    /** リスナーは常に強参照しなければならない */
    private Consumer<T>listener;
    
    Ref(Consumer<T>listener) {
      this.listener = listener;
    }
    
    /** 参照対象オブジェクトを取得する。nullの場合がある。これはリスナー削除の時にも使用される */
    public abstract Object getObject();
    
    /** 参照対象オブジェクトのクラスを取得する。デバッグ用。nullの場合がある */
    public abstract Class<?>getClazz();
    
    /** リスナーを取得する */
    public Consumer<T>getListener() {
      return listener;
    }
    
    /** デバッグ用の文字列化 */
    @Override
    public String toString() {
      String className = "null";
      if (getClazz() != null) className = getClazz().getName();
      return className + ", " + getListener();
    }
  }
  
  /** リスナーへの強参照 */
  static class Strong<T> extends Ref<T> {
    
    /** 対象となるオブジェクト。nullの場合がある。nullの場合はハンドラ削除はできない */
    private Object object;
    
    /** 対象オブジェクトとハンドラを指定する */
    public Strong(Object object, Consumer<T>handler) {
      super(handler);
      this.object = object;
    }
    
    @Override
    public Object getObject() {
      return object;
    }
    
    @Override
    public Class<?>getClazz() {
      return object == null ? null:object.getClass();
    }
    
    /** デバッグ用の文字列化 */
    @Override
    public String toString() {
      return "Strong:" + super.toString();
    }
  }

  /**
   * 弱参照でリスナーを保持するが、実際に弱参照するのはリスナーを持つ「オブジェクト」
   * リスナーと共にオブジェクトが指定されなければならない。
   * @author ysugimura
   *
   * @param <T>
   */
  static class Weak<T> extends Ref<T> {
    
    /** 対象オブジェクトを保持する弱参照 */
    WeakReference<Object>holder;
    
    /** 対象オブジェクトのクラス。デバッグ用 */
    Class<?>clazz;
    
    public Weak(Object object, Consumer<T>value) {
      super(value);
      holder = new WeakReference<Object>(object);
      clazz = object.getClass();
    }

    /** 
     * オブジェクトが消えていたらリスナーも消えているものとする 
     */
    public Consumer<T>getListener() {
      if (getObject() != null) return super.getListener();
      return null;
    }
    
    /**
     * 弱参照されているオブジェクトを取得する。消えている場合はnullを返す。
     */
    public Object getObject() {
      return holder.get();
    }

    /** オブジェクトのクラスを返す。デバッグ用 */
    public Class<?>getClazz() {
      return clazz;
    }
    
    @Override
    public String toString() {
      return "Weak:" + super.toString();
    }
  }
}
