package com.cm55.eventBus;

import static org.mockito.Mockito.*;

import java.util.function.*;

import org.junit.*;
import org.mockito.*;
import org.mockito.internal.verification.*;

public class UnlistenerTest {

  static class Event {    
  }
  
  @Mock ListenerHolder<Event> holder;
  
  @Before
  public void before() {
    MockitoAnnotations.initMocks(this);
  }
  
  @Test
  public void test1() {
    Consumer<Event>listener = e->{};
    Unlistener<Event> un = new Unlistener<Event>(holder, listener);
    un.unlisten();
    un.unlisten();
    verify(holder, new Times(1)).removeListener(listener);
  }
  
  @Test
  public void test2() {
    Consumer<Event>listener = e->{};
    Unlistener<Event> un = new Unlistener<Event>(holder, listener);
    Unlistener.execute(un);
    Unlistener.execute(un);
    verify(holder, new Times(1)).removeListener(listener);
  }
  
  @Test
  public void test3() {
    Unlistener.execute(null);
  }

}
