package com.cm55.eventBus;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class EventTypeTest {

  public static class Foo {    
  }
  public static class Bar<T> {    
  }
  
  @Test
  public void test1() {
    EventType<Bar<Foo>>type = new EventType<Bar<Foo>>() {};
    assertThat(type.getRawType(), equalTo(Bar.class));
  }
  
  @Test
  public void test2() {
    EventType<Foo>type = new EventType<Foo>() {};
    assertThat(type.getRawType(), equalTo(Foo.class));
  }
}
