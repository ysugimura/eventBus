package com.cm55.eventBus;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import org.apache.commons.logging.*;

/**
 * 任意のイベントクラスと、それを受け入れるリスナーを登録しておき、イベントが発生したら登録されたすべてのリスナーに
 * 通知する。
 * <p>
 * 例えば
 * </p>
 * <pre>
 * static class SomeEvent {
 * }
 * EventBus bus = new EventBus();
 * bus.listen(SomeEvent.class, e-> { ... });
 * </pre>
 * <p>
 * としておき、bus.dispatchEvent(new SomeEvent());としてイベントを発生させる。
 * </p>
 * <p>
 * なお、イベントはクラス階層として構成でき、リスナー側もその階層の任意の部分以下をリッスンすることができる。例えば、
 * </p>
 * <pre>
 * static class SomeEvent {}
 * static class FooEvent extends SomeEvent {}
 * static class BarEvent extends SomeEvent {}
 * ...
 * bus.listen(SomeEvent.class, e-> { ... });
 * </pre>
 * <p>
 * とすることで、例えば、bus.dispatchEvent(new FooEvent());でもリスナーが呼び出される。
 * </p>
 * @author ysugimura
 */
public class EventBus {

  protected static Log log = LogFactory.getLog(EventBus.class);
  protected static CallerStack callerStack = new CallerStack(EventBus.class);
  
  /** イベントタイプ/リスナーホルダマップ */
  private Map<Class<?>, ListenerHolder<?>> listenerHolderMap;

  /** ディスパッチフック */
  private Optional<Function<Object, Object>>dispatchHook = Optional.empty();
  
  /**
   * 強参照でリスナーを保持する。
   * @param eventType イベントタイプ
   * @param listener リスナー
   * @return {@link Unlistener}
   */
  public <T> Unlistener<T> listen(Class<T> eventType, Consumer<T>listener) {
    if (log().isTraceEnabled()) log().trace("listen " + eventType + " from " + callerStack.get());
   return addListener(false, eventType, null, listener);
  }
  
  /**
   * 強参照でリスナーを保持する。イベントタイプは{@link EventType}のインスタンスとして与える。
   * {@link EventType}を参照のこと。
   * @param eventType イベントタイプ
   * @param listener リスナー
   * @return {@link Unlistener}
   */
  @SuppressWarnings("unchecked")
  public <T> Unlistener<T> listen(EventType<T> eventType, Consumer<T>listener) {
    if (log().isTraceEnabled()) log().trace("listen " + eventType + " from " + callerStack.get());
   return addListener(false, (Class<T>)eventType.getRawType(), null, listener);
  }
  
  /**
   * 弱参照でリスナーを保持する。オブジェクトの指定が必須。
   * 返された{@link Unlistener}で強制的にリスナー解除を行うこともできる。
   * @param eventType イベントタイプ
   * @param object オブジェクト
   * @param listener リスナー
   * @return {@link Unlistener}
   */
  public <T> Unlistener<T> listenWeak(Class<T> eventType, Object object, Consumer<T> listener) {
    if (log().isTraceEnabled()) log().trace("listenWeak " + eventType + " from " + callerStack.get());
    return addListener(true, eventType, object, listener);
  }
  
  /**
   * 弱参照でリスナーを保持する。オブジェクトの指定が必須。
   * 返された{@link Unlistener}で強制的にリスナー解除を行うこともできる。
   * イベントタイプは{@link EventType}のインスタンスとして与える。
   * {@link EventType}を参照のこと。
   * @param eventType イベントタイプ
   * @param object オブジェクト
   * @param listener リスナー
   * @return {@link Unlistener}
   */
  @SuppressWarnings("unchecked")
  public <T> Unlistener<T> listenWeak(EventType<T> eventType, Object object, Consumer<T> listener) {
    if (log().isTraceEnabled()) log().trace("listenWeak " + eventType + " from " + callerStack.get());
    return addListener(true, (Class<T>)eventType.getRawType(), object, listener);
  }
  
  /**
   * 指定タイプのイベントのリスナーを登録する。登録解除用の{@link Unlistener}を返す。
   * @param weak
   * @param eventType
   * @param object
   * @param listener
   * @return
   */
  @SuppressWarnings("unchecked")
  private <T> Unlistener<T> addListener(boolean weak, Class<T> eventType, Object object, Consumer<T> listener) {
    if (listenerHolderMap == null) listenerHolderMap = new HashMap<Class<?>, ListenerHolder<?>>();
    ListenerHolder<T>holder = (ListenerHolder<T>)listenerHolderMap.get(eventType);
    if (holder == null) {
      holder = new ListenerHolder<T>(eventType);
      listenerHolderMap.put(eventType, holder);
    }
    holder.addListener(weak, object, listener);
    return new Unlistener<T>(holder, listener);
  }

  /** 
   * イベントをディスパッチする。
   * <p>
   * このとき、このオブジェクトのクラスと同一のイベントタイプのリスナーだけではなく、その上位クラスのイベントタイプのリスナーが登録されていれば
   * そのリスナーにも通知される。
   * </p>
   * <pre>
   * static class SomeEvent {}
   * static class FooEvent extends SomeEvent {}
   * </pre>
   * <p>
   * の場合にSomeEventのリスナーはFooEventのリスナーにもディスパッチ対象となる。
   * </p>
   * @param event ディスパッチするイベントオブジェクト
   */
  @SuppressWarnings("unchecked")
  public <T>void dispatchEvent(T inputEvent) {
    
    if (log().isTraceEnabled())
      log().trace("dispatchEvent " + inputEvent.getClass() + " from " + callerStack.get());

    // dispatchHookのある場合、それを通す
    T event = (T)dispatchHook.map(h->h.apply(inputEvent)).orElse(inputEvent);
    
    if (listenerHolderMap == null) return;

    // ディスパッチ途中でマップが変更されることがあるため、いったんリスト化している
    listenerHolderMap.entrySet().stream()
      .filter(entry->((Class<?>)entry.getKey()).isInstance(event)) // イベントを代入可能なクラスのみとする
      .map(entry->entry.getValue()).collect(Collectors.toList()) // ListenerHolderをいったんリストにまとめる
      .stream().forEach(holder-> { // ディスパッチする
        ((ListenerHolder<? super T>)holder).getListeners()
          .forEach(listener->listener.accept(event));
      });
  }
  
  /**
   * ディスパッチをフックし、イベントを調査・変更できる。
   * @param dispatchHook
   */
  public void setDispatchHook(Function<Object, Object>dispatchHook) {
    this.dispatchHook = Optional.ofNullable(dispatchHook);
  }
  
  protected Log log() {
    return log;
  }
}

