package com.cm55.eventBus;

import java.util.function.*;

import org.apache.commons.logging.*;

/**
 * {@link EventBus}に登録されたリスナーを解除するためのオブジェクト
 * @author ysugimura
 *
 * @param <T>
 */
public class Unlistener<T> {

  protected static Log log = LogFactory.getLog(Unlistener.class);
  protected static CallerStack callerStack = new CallerStack(Unlistener.class);
  
  /** リスナーが登録されたホルダ */
  private ListenerHolder<T> holder;
  
  /** リスナー */
  private final Consumer<T> listener;
  
  /**
   * ホルダとリスナーを指定する
   * @param holder
   * @param listener
   */
  Unlistener(ListenerHolder<T>holder, Consumer<T>listener) {
    this.holder = holder;
    this.listener = listener;
  }

  /**
   * 登録を解除する
   */
  public void unlisten() {
    if (holder == null) return;
    if (log().isTraceEnabled()) log().trace("unlisten " + holder.eventType + " from " + callerStack.get());
    holder.removeListener(listener);
    holder = null;
  }

  /**
   * 指定された{@link Unlistener}の登録を解除するが、nullであれば何もしない
   * @param unlistener
   */
  public static void execute(Unlistener<?> unlistener) {
    if (unlistener == null) return;
    unlistener.unlisten();
  }
  
  protected Log log() {
    return log;
  }
}
