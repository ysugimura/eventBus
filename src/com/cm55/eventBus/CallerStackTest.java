package com.cm55.eventBus;

import static org.junit.Assert.*;

import java.util.*;
import java.util.stream.*;

import org.junit.*;

import com.cm55.eventBus.CallerStackTest.Sample.*;

public class CallerStackTest {
  
  public static class Sample {
    static List<String>list = new ArrayList<>();
    CallerStack callerStack = new CallerStack(Sample.class, FooBar.class);
    public void sample() {
      list.add(callerStack.get());
    }
    public static class FooBar {
      void test() {
        new Sample().sample();
      }
    }
    static String getResult() {
      return Sample.list.stream().collect(Collectors.joining("\n"));
    }
  }

  public static class Foo {
    void test() {
      new Sample().sample();
    }
  }
  
  public static class Bar {
    void test() {
      new FooBar().test();
    }
  }
  
  @Test
  public void test() {
    new Foo().test();
    new Bar().test();
    
    assertEquals(
      "com.cm55.eventBus.CallerStackTest$Foo.test(CallerStackTest.java:32)\n" + 
      "com.cm55.eventBus.CallerStackTest$Bar.test(CallerStackTest.java:38)",
      Sample.getResult());
  }
}
