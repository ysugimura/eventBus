package com.cm55.eventBus;


import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Suite.*;


@RunWith(Suite.class)
@SuiteClasses( { 
  CallerStackTest.class,
  EventBusTest.class,
  EventBusEventTypeTest.class,
  EventTypeTest.class,
  ListenerHolderTest.class,
  UnlistenerTest.class
  
})
public class AllTest {
  public static void main(String[] args) {
    JUnitCore.main(AllTest.class.getName());
  }
}
