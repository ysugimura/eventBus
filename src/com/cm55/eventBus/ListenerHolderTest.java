package com.cm55.eventBus;

import static org.junit.Assert.*;

import java.util.*;
import java.util.function.*;

import org.junit.*;

public class ListenerHolderTest {

  static class Event {    
  }
  static class Sample {    
  }
  
  ListenerHolder<Event>et;
  List<Event>list = new ArrayList<>();
  
  @Before
  public void before() {
    et = new ListenerHolder<>(Event.class);
  }
  
  @Test
  public void 強参照テスト() {
    Consumer<Event>listener = e->{};
    et.addListener(false, null, listener);  
    assertEquals(1, et.listenerList.size());
    System.gc();
    System.gc();
    assertEquals(1, et.listenerList.size());
    et.removeListener(listener);
    assertEquals(0, et.getListeners().size());
  }

  @Test
  public void 弱参照テスト() {
    Consumer<Event>listener = e->{};
    et.addListener(true, new Sample(), listener);  
    assertEquals(1, et.listenerList.size());
    System.gc();
    System.gc();
    assertEquals(0, et.getListeners().size());
  }
  
  @Test
  public void test2() {
    try {
      et.addListener(true,  null,  e->{});
      fail();
    } catch (NullPointerException ex) {}
  }
  
  @Test
  public void test3() {
    Consumer<Event>l = e->{};
    et.addListener(false, null, l);    
    et.addListener(true,  new Sample(), e->{});
    assertEquals(2, et.getListeners().size());
    System.gc();
    System.gc();
    assertEquals(1, et.getListeners().size());
  }
  

}
