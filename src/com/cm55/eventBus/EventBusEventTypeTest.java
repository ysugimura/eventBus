package com.cm55.eventBus;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

/**
 * {@link EventType}のテスト
 * @author ysugimura
 */
public class EventBusEventTypeTest {

  public static class Foo {    
    final String name;
    Foo(String name) {
      this.name = name;
    }
  }
  public static class Bar<T> {    
    final T object;
    Bar(T object) {
      this.object = object;
    }
  }

  EventBus eventBus;
  
  @Before
  public void before() {
    eventBus = new EventBus();
  }
  
  @Test
  public void test() {
    List<String>result = new ArrayList<String>();
    
    // EventTypeを使用することにより、キャスト無しにFoo#nameへのアクセスが可能になる。
    eventBus.listen(new EventType<Bar<Foo>>() {}, e->result.add(e.object.name));
    
    // イベントを発火
    eventBus.dispatchEvent(new Bar<>(new Foo("test")));
    
    assertThat(result.size(), equalTo(1));
    assertThat(result.get(0), equalTo("test"));
  }
}
