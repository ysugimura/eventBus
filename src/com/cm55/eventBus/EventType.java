package com.cm55.eventBus;

import java.lang.reflect.*;

/**
 * 型パラメータを外見上保持するためのオブジェクト。
 * このクラスは常に内部クラスとして作成され、{@link EventBus#listen(EventType, java.util.function.Consumer)}メソッドに指定される。
 * 例えば以下のように指定する。
 * <pre>
 * public static class Foo {}
 * public static class Bar<T> {}
 * ...
 * eventBus.listen(new EventType<Bar<Foo>>() {}, l-> ....);
 * </pre>
 * <p>
 * この機能が必要な理由としては、上記のようなケースの場合、以下のように記述することができないからである。
 * </p>
 * <pre>
 * eventBus.listen(Bar<Foo>.class, l-> ...);
 * </pre>
 * <p>
 * Javaではこのような書き方ができず、このメソッドを使う限りでは、以下のように書くしかない。
 * </p>
 * <pre>
 * eventBus.listen(Bar.class, l-> ...);
 * </pre>
 * <p>
 * しかし、この書き方では、Barの型パラメータであるFooの情報が落ちてしまい、リスナーの処理ではキャストを使うことになる。
 * </p>
 * @author ysugimura
 *
 * @param <T>
 */
public abstract class EventType<T> {
  
  /** 型パラメータTとして与えられたクラスのRawタイプ */
  final Class<?> rawType;

  protected EventType() {
    // 元々の{@link EventType}を取得する。本オブジェクトは常にサブクラス化されるので、ここで取得されるのは{@link EventType}になる。
    ParameterizedType superType = (ParameterizedType)this.getClass().getGenericSuperclass();
    
    // EventTypeの一番目（唯一）の型パラメータの型を取得する。つまり、new EventType<Foo>() {}の「Foo」部分
    Type argType = superType.getActualTypeArguments()[0];
        
    // Fooがパラメータ付、つまりFoo<Bar>の形のとき
    if (argType instanceof ParameterizedType) {
      ParameterizedType paramType = (ParameterizedType)argType;
    
      // 上記の型パラメータのRawタイプを取得する。つまり、Tとして与えられた型のRawタイプになる。
      rawType = (Class<?>)paramType.getRawType();
    } else {
      // Fooがパラメータ付でないとき
      rawType = (Class<?>)argType;
    }    
  }

  /**
   * 型パラメータTとして与えられたクラスのRawタイプを取得する
   * @return 型パラメータTとして与えられたクラスのRawタイプ
   */
  public Class<?>getRawType() {
    return rawType;
  }
}
