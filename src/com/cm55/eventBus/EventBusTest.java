package com.cm55.eventBus;

import static org.junit.Assert.*;

import java.util.*;
import java.util.stream.*;

import org.junit.*;

public class EventBusTest {

  static class SomeEvent {}
  static class ParentEvent  {}
  static class ChildEvent extends ParentEvent {}
  
  @Test
  public void addTest() {
    EventBus bus = new EventBus();
    
    List<Object>list = new ArrayList<>();  
    
    Unlistener<SomeEvent>un = bus.listen(SomeEvent.class, e->list.add(e));
    bus.listen(SomeEvent.class, e->{});
    bus.dispatchEvent(new SomeEvent());
    bus.dispatchEvent(new ChildEvent());
    bus.dispatchEvent(new ParentEvent());
    
    un.unlisten();
    bus.dispatchEvent(new SomeEvent());
    validate(list, SomeEvent.class);
  }
  
  @Test
  public void removeTest() {
    EventBus bus = new EventBus();    
    boolean[]value = new boolean[1];
    Unlistener<SomeEvent>un = bus.listen(SomeEvent.class, e-> { value[0] = true; });
    un.unlisten();
    bus.dispatchEvent(new SomeEvent());
    assertFalse(value[0]);
  }
  
  @Test
  public void addRemoveTest() {
    List<Object>list1 = new ArrayList<>();  
    List<Object>list2 = new ArrayList<>();  
    EventBus bus = new EventBus();
    
    Unlistener<SomeEvent>un = bus.listen(SomeEvent.class,  e-> list1.add(e));
    bus.listen(ParentEvent.class,  e-> list1.add(e));
    
    bus.listen(SomeEvent.class,  e->list2.add(e));
    bus.listen(ChildEvent.class,  e-> list2.add(e));
    
    bus.dispatchEvent(new SomeEvent());
    bus.dispatchEvent(new ParentEvent());
    bus.dispatchEvent(new ChildEvent());
    
    validate(list1, SomeEvent.class, ParentEvent.class, ChildEvent.class);
    validate(list2, SomeEvent.class, ChildEvent.class);

    un.unlisten();
    bus.dispatchEvent(new SomeEvent());
    
    validate(list1, SomeEvent.class, ParentEvent.class, ChildEvent.class);
    validate(list2, SomeEvent.class, ChildEvent.class, SomeEvent.class);
  }

  void validate(List<Object>list, Class<?>...classes) {
    assertArrayEquals(classes, 
        list.stream().map(o->o.getClass()).collect(Collectors.toList()).toArray(new Class[0]));
  }
  
  @Test
  public void weakTest() throws Exception {
    EventBus bus = new EventBus();
    
    {
      List<Object>list1 = new ArrayList<>();
      bus.listenWeak(SomeEvent.class, new Sample(), e->list1.add(e));    
      bus.dispatchEvent(new SomeEvent());
      validate(list1, SomeEvent.class);
    }
    
    {
      List<Object>list1 = new ArrayList<>();
      bus.listenWeak(SomeEvent.class, new Sample(), e->list1.add(e));  
      System.gc();
      System.gc();
      bus.dispatchEvent(new SomeEvent());
      assertEquals(0, list1.size());
    }

  }
  
 

  static class Sample {
  }
  
  
}
